def g(x, i):
    if i == 1 :
        return 2 * x
    else:
        fact = 1
        o = i
        while o > 0:
            fact = fact * o
            o = o - 1
        return ((2**i)*(x**i))/ fact
